<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExchangeImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange_images', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('exchange_post_id')->unsigned();
            $table->foreign('exchange_post_id')->references('id')->on('exchange_posts');
            $table->string('exchange_image_1')->nullable();
            $table->string('exchange_image_2')->nullable();
            $table->string('exchange_image_3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange_images');
    }
}
