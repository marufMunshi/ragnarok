<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories');
            $table->integer('language_id')->unsigned();
            $table->foreign('language_id')->references('id')->on('languages');
            $table->unique(array('title', 'category_id', 'language_id'));
            $table->timestamps();
        });

        Schema::create('book_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('book_id')->unsigned();
            $table->foreign('book_id')->references('id')->on('books');
            // true means public
            $table->boolean('book_public_status')->default(true);
            // false means book not lended to any borrower
            $table->boolean('book_lending_status')->default(false);
            $table->boolean('book_sell_status')->default(false);
            $table->boolean('book_sold')->default(false);
            $table->boolean('book_exchange_status')->default(false);
            $table->boolean('book_exchanged')->default(false);
            // $table->integer('number_of_books');
            $table->string('image')->nullable();
            $table->date('purchased_date')->nullable();
            $table->integer('price')->nullable();
            $table->date('published_date')->nullable();
            $table->string('edition')->nullable();
            $table->string('publisher')->nullable();
            $table->unique(array('user_id', 'book_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
