<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'user_name', 'image', 'email', 'password', 'provider',
        'provider_id', 'mobile', 'address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles() {
      return $this->belongsTo(Role::class);
    }

    public function hasRole($role_name) {
        // dd($this->role_id);
        $name = DB::table('roles')->where('id', '=', $this->role_id)->get();
        // dd($role_name == $name[0]->name);
        if($role_name == $name[0]->name)
        {
            return true;
        }
        else
        {
            return false;
        }
        // return false;
    }

    public function books() {
        return $this->belongsToMany(Book::class)
        ->withPivot('book_public_status', 'book_lending_status', 'price',
        'number_of_books', 'purchased_date', 'image');
    }

    public function ratings() {
        return $this->hasMany(Rating::class);
    }

    public function reviews() {
        return $this->hasMany(Review::class);
    }

    public function sellPosts() {
        return $this->hasMany(SellPost::class);
    }

    public function exchangePosts() {
        return $this->hasMany(ExchangePost::class);
    }


}
