<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $fillable = [
        'author_name', 'author_bio', 'author_image'
    ];

    public function books() {
        return $this->belongsToMany(Book::class);
    }
}
