<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExchangeImage extends Model
{
    protected $fillable = [
        'exchange_post_id', 'exchange_image_1',
        'exchange_image_2', 'exchange_image_3'
    ];

    public function exchangePost() {
        return $this->belongsTo(ExchangePost::class);
    }
}
