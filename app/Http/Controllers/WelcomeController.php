<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\SellPost;
use App\ExchangePost;
use App\Review;

class WelcomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sellPosts = SellPost::where('sell_status', false)->take(4)->get();
        $exchangePosts = ExchangePost::where('exchange_status', false)->take(4)->get();
        $reviews = Review::latest()->take(4)->get();

        // return $exchangePosts;
        return view('welcome', compact('sellPosts', 'exchangePosts', 'reviews'));
    }
}
