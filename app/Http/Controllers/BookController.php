<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Language;
use Auth;
use App\User;
use Carbon\Carbon;
use DB;

use \App\Book;
use \App\Author;
use \App\ExchangePost;
use \App\SellPost;

class BookController extends Controller
{
    public function index()
    {
        return view('dashboard.all-books');
    }

    public function show(Request $request, $bookId, $userId)
    {
        $book = Book::find($bookId);
        $user = User::find($userId);
        $reviews = $book->reviews;
        $note = ExchangePost::where(['user_id' => $userId, 'book_id' => $bookId])->pluck('exchange_post');
        $price = SellPost::where(['user_id' => $userId, 'book_id' => $bookId])->pluck('price');
        $bookStatus = DB::table('book_user')->where(['book_id' => $bookId, 'user_id' => $userId])->get();

        return view('book', compact('book', 'reviews', 'user', 'note', 'bookStatus', 'price'));
    }

    public function shelf()
    {
        $books = Book::with('users')
                    ->whereHas('users', function($q) {
                                   $q->where('user_id', Auth::id());
                        })
                    ->latest()
                   ->paginate(20);
        return view('dashboard.shelf', compact('books'));
    }
    
    public function create()
    {
        return view('dashboard.create-book');
    }

    public function dashboardShow(Request $request, $id)
    {
        $book = Book::with('users')
                    ->where('id', $id)
                    ->whereHas('users', function($q) {
                                $q->where('user_id', Auth::id());
                        })
                   ->get();
        $borrowers = $book[0]->borrowers;
        return view('dashboard.book', compact('book', 'borrowers'));
    }

    public function bookName() {
        return Book::with('category', 'language', 'authors')->get();
    }

    public function showWithoutUser(Request $request, $id)
    {
        $book = Book::find($id);
        $reviews = $book->reviews;
        return view('book-non-user', compact('book', 'reviews'));
    }

    public function store(Request $request)
    
    {
        
        $this->validate($request, [
            'title' => 'required|string',
            'author' => 'required|string',
            'category' => 'required|string',
            'language' => 'required|string',
            'book_option' => 'required|string',
            ]);
            
        if($request->has('book_cover')) {
            $image = $request->file('book_cover');
			$fileName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images/books'), $fileName);
        } else {
            $fileName = NULL;
        }

        $user = Auth::user(); // current user instance

        // this function add new author in author table
        // if author existed returned id
        $author = Author::firstOrNew([
            'author_name' => $request->input('author')
        ]);

        if ($author->id == null) {
            $author->save();
        }

        $category = Category::firstOrNew([
            'category_name' => $request->input('category')
        ]);

        if ($category->id == null) {
            $category->save();
        }

        $language = Language::firstOrNew([
            'language_name' => $request->input('language')
        ]);

        if ($language->id == null) {
            $language->save();
        }

        $book = Book::firstOrNew([
            'title' => $request->input('title'),
            'category_id' => $category->id,
            'language_id' => $language->id
        ]);


        if ($book->id == null) {
            $book->save();
        }

        $author_book_pivot = DB::table('author_book')->where([
            ['author_id', '=', $author->id],
            ['book_id', '=', $book->id],
        ])->count();

        // return $author_book_pivot;

        if ($author_book_pivot == 0)
        {
            $author->books()->attach($book); // inserting data in the pivot table
        }

        $book_user_pivot = DB::table('book_user')->where([
            ['user_id', '=', $user->id],
            ['book_id', '=', $book->id],
        ])->count();

        

        if ($book_user_pivot == 0) {

            $user->books()->attach($book);
            $book->users()->updateExistingPivot($user->id, [
                'image' => $fileName
            ]);

            if ($request->input('book_option') == 'sell') {
                return redirect()->route('create-sell', ['id' => $book->id]);
            } elseif ($request->input('book_option') == 'exchange') {
                return redirect()->route('create-exchange', ['id' => $book->id]);            
            } elseif ($request->input('book_option') == 'lend') {
                return redirect()->route('create-borrower', ['id' => $book->id]);            
            } else {
                // for success notification
                $request->session()->flash('status', 'Book added successfully!');
                return redirect()->route('shelf');
            }
            
        }
        else {
            $request->session()->flash('error', 'This book is already exist in the shelf!');
            return redirect()->back();
        }

    }

    public function changeOption(Request $request)
    {
        $this->validate($request, [
            'book_id' => 'required',
            'current_option' => 'required',
            'book_option' => 'required'
        ]);

        $bookId = $request->input('book_id');
        $userId = $request->user()->id;

        if ($request->input('current_option') == 'sell') {

            /*
                Delete Current Option
            */

            //Change Sell Status
            Book::find($bookId)->users()->updateExistingPivot($userId, [
                'book_sell_status' => false
            ]);

            //Delete Sell Post
            SellPost::where([
                'book_id' => $bookId,
                'user_id' => $userId
            ])->delete();

            /*
                Act according to book option input
            */
            if ($request->input('book_option') == 'exchange') {
                return redirect()->route('create-exchange', ['id' => $bookId]);
            }
            
            if ($request->input('book_option') == 'lend') {
                return redirect()->route('create-borrower', ['id' => $bookId]);
            }

            if ($request->input('book_option') == 'shelf') {
                $request->session()->flash('status', 'Book back on shelf successfully!');
                return redirect()->route('shelf');
            }
        }

        if ($request->input('current_option') == 'exchange') {
            /*
                Delete Current Option
            */

            //Change Exchange Status
            Book::find($bookId)->users()->updateExistingPivot($userId, [
                'book_exchange_status' => false
            ]);

            //Delete Exchange Post
            ExchangePost::where([
                'book_id' => $bookId,
                'user_id' => $userId
            ])->delete();

            /*
                Act according to book option input
            */
            if ($request->input('book_option') == 'sell') {
                return redirect()->route('create-sell', ['id' => $bookId]);
            }
            
            if ($request->input('book_option') == 'lend') {
                return redirect()->route('create-borrower', ['id' => $bookId]);
            }

            if ($request->input('book_option') == 'shelf') {
                $request->session()->flash('status', 'Book back on shelf successfully!');
                return redirect()->route('shelf');
            }
        }

        if ($request->input('current_option') == 'shelf') {

            if ($request->input('book_option') == 'sell') {
                return redirect()->route('create-sell', ['id' => $bookId]);
            } 

            if ($request->input('book_option') == 'exchange') {
                return redirect()->route('create-exchange', ['id' => $bookId]);            
            }

            if ($request->input('book_option') == 'lend') {
                return redirect()->route('create-borrower', ['id' => $bookId]);            
            } 
        }

        if ($request->input('current_option') == 'lend') {

             $this->validate($request, [
                'return_date' => 'required|date' //Only accept if return date > lend date
            ]);

            $return_date = $request->input('return_date');
		    $fomatted_return_date = Carbon::parse($return_date)->format('Y-m-d');

            //Change Exchange Status
            Book::find($bookId)->users()->updateExistingPivot($userId, [
                'book_lending_status' => false
            ]);

            //Insert Return Date
            Book::find($bookId)->borrowers()->updateExistingPivot($userId, [
                'orginal_return_date' => $fomatted_return_date
            ]);

            $request->session()->flash('status', 'Book back on shelf successfully!');
            return redirect()->route('shelf');
        }
    }

    public function updateExtraInfo(Request $request)
    {
        $this->validate($request, [
            'edition' => 'required',
            'publisher' => 'required',
            'book_id' => 'required'
        ]);

        $book_id = $request->input('book_id');
        $user_id = $request->user()->id;

        // for updating pivot table according to the user and book id
        User::find($user_id)->books()->updateExistingPivot($book_id, [
            'edition' => $request->input('edition'),
            'publisher' => $request->input('publisher')
        ]);

        $request->session()->flash('status', 'Book updated successfully!');
        return redirect()->back();
    }
}
