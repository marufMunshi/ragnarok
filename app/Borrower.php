<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Borrower extends Model
{
    protected $fillable = [
        'borrower_name', 'phone_number', 'email', 'user_id'
    ];

    public function books() {
        return $this->belongsToMany(Book::class)
        ->withPivot('borrower_id', 'book_id', 'lend_date', 'return_date',
            'orginal_return_date');
    }

    public function user() {
        return $this->belongsTo(User::Class);
    }
}
