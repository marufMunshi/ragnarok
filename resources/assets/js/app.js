
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import TweenMax from "gsap";
// import ScrollMagic from 'scrollmagic'

// window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));

// const app = new Vue({
//     el: '#app'
// });

const bookshelfImg = document.getElementById('bookshelf-svg');
const bookshelfImgBooks = document.querySelectorAll('.bookshelf-svg-books');
const reviewStars = document.querySelectorAll('.review-star');
const openBookImg = document.getElementById('open-book-svg');
const sellImg = document.getElementById('sell-svg');
const howItWorksDescription = document.querySelector('.how-it-works-description');
const virtualLibrary = document.getElementById('how-it-works-virtual-library');
const writeReview = document.getElementById('how-it-works-write-review');
const sellExchange = document.getElementById('how-it-works-sell-exchange');
const sellRightArrow = document.getElementById('sell-svg-right-arrow');
const sellLeftArrow = document.getElementById('sell-svg-left-arrow');
const sellRightArrowHead = document.getElementById('sell-svg-right-arrow-head');
const sellLeftArrowHead = document.getElementById('sell-svg-left-arrow-head');


const clearTl = new TimelineMax();

clearTl
    // .set(bookshelfImg, { autoAlpha: 0 })
    .set(bookshelfImgBooks, { autoAlpha: 0 })
    .set(reviewStars, { autoAlpha: 0 })
    .set(openBookImg, { autoAlpha: 0 })
    .set(sellImg, { autoAlpha: 0 })
    .set(virtualLibrary, { autoAlpha: 0 })
    .set(writeReview, { autoAlpha: 0 })
    .set(sellExchange, { autoAlpha: 0 })
    .set(sellRightArrow, { fill: '#fff' })
    .set(sellLeftArrow, { fill: '#fff' })
    .set(sellRightArrowHead, { autoAlpha: 0 })
    .set(sellLeftArrowHead, { autoAlpha: 0 })
    ;

const parallaxTl = new TimelineMax();
parallaxTl
    .to(bookshelfImg, 0.1, { autoAlpha: 1, ease: Power0.easeNone })
    .to(virtualLibrary, 0.1, { autoAlpha: 1, ease: Power0.easeNone }, '-=0.1')
    .staggerTo(bookshelfImgBooks, 0.5, { autoAlpha: 1 })
    .to(bookshelfImg, 0.1, { autoAlpha: 0, ease: Power0.easeNone })
    .to(virtualLibrary, 0.1, { autoAlpha: 0, ease: Power0.easeNone }, '-=0.1')
    .to(openBookImg, 0.1, { autoAlpha: 1, ease: Power0.easeNone })
    .to(howItWorksDescription, 0.1, { backgroundColor: '#7ABEE3' }, '-=0.1')
    .to(writeReview, 0.1, { autoAlpha: 1, ease: Power0.easeNone })
    .staggerFromTo(reviewStars, 0.3, { scale: 0, autoAlpha: 0, transferOrigin: 'center bottom'},
        { scale: 1, autoAlpha: 1, transferOrigin: 'center bottom' }, 0.1)
    .to(openBookImg, 0.1, { autoAlpha: 0, ease: Power0.easeNone })
    .to(writeReview, 0.1, { autoAlpha: 0, ease: Power0.easeNone }, '-=0.1')
    .to(sellImg, 0.1, { autoAlpha: 1, ease: Power0.easeNone })
    .to(howItWorksDescription, 0.1, { backgroundColor: '#7383C8' })    
    .to(sellExchange, 0.1, { autoAlpha: 1, ease: Power0.easeNone }, '-=0.1')
    .to(sellRightArrow, 0.5, { fill: '#C7CFE2', ease: Power0.easeNone }, '-=0.1')
    .to(sellRightArrowHead, 0.1, { autoAlpha: 1, ease: Power0.easeNone })
    .to(sellLeftArrow, 0.5, { fill: '#C7CFE2', ease: Power0.easeNone })
    .to(sellLeftArrowHead, 0.1, { autoAlpha: 1, ease: Power0.easeNone })
    ;

const controller = new ScrollMagic.Controller();

const pinHowItWorksImage = new ScrollMagic.Scene({
    triggerElement: '#how-it-works',
    triggerHook: 0,
    duration: 500
})
    .setPin('.pinned-hiw-sec-one')
    .setTween(parallaxTl)
    // .addIndicators({
    //     name: 'pin-how-it-works',
    //     colorTrigger: 'black',
    //     indent: 200,
    //     colorStart: '#75C695',
    //     colorEnd: '#75C625',
    // })
    .addTo(controller);



// //Smooth scrolling.....
$('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function (event) {
        // On-page links
        if (
            location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
            location.hostname == this.hostname
        ) {
            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000, function () {
                    // Callback after animation
                    // Must change focus!
                    var $target = $(target);
                    $target.focus();
                    if ($target.is(":focus")) { // Checking if the target was focused
                        return false;
                    } else {
                        $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                        $target.focus(); // Set focus again
                    };
                });
            }
        }
    });




$("#review-1, #review-2, #review-3").hide();

$("#review-nav-0").addClass('active');

$(".book-link").click(function(e) {
  e.preventDefault();

  let value = $(this)
    .parent()
    .attr("id")
    .split("-")[2];

  $("#review-nav-0, #review-nav-1, #review-nav-2, #review-nav-3").removeClass(
    "active"
  );
  $(this)
    .parent()
    .addClass("active");

  $("#review-0, #review-1, #review-2, #review-3").hide();

  let id = "#review-" + value;

  $(id).show();

  console.log(id);
});