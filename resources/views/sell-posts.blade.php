@extends('layouts.master')

@section('title', 'Book')

@section('stylesheet')
	@parent
@endsection

@section('content')

    <section id="sell-exchange-books">
        <div class="container">
            <div class="row">

                <div class="section-heading text-center text-capitalize">
                    <h1>New Sell Post</h1>
                </div>

                <div class="row">
                    <div class="col-xs-6 col-sm-4 col-md-3">

                        @foreach ($sellPosts as $sellPost)

                            <div class="book-wrapper">
                                <a href="">
                                    <div class="book-image">
                                        <img class="img-responsive" src="/images/default-cover.jpg" alt="book cover" />
                                    </div>
                                    <div class="book-meta text-center text-capitalize">
                                        <p>
                                            <em>৳</em>{{ $sellPost->price }}
                                        </p>
                                        <div class="star-rating">
                                            @for ($i = 0; $i < $sellPost->book->ratings->avg('value'); $i++)
                                                <i class="ion-ios-star"></i>
                                            @endfor
                                        </div>
                                        <p>{{ $sellPost->book->title }}</p>
                                    </div>

                                </a>
                                <div class="book-buttons">
                                    <a href="{{ route('book', ['book-id' => $sellPost->book_id, 'user-id' => $sellPost->user_id ])}}">Buy Now</a>
                                </div>
                            </div>

                        @endforeach

                    </div>

                    
                </div>

                <div class="row">
                    <div class="col-md-12 text-center">
                        {{ $sellPosts->links() }}
                    </div>
                </div>

            </div>
        </div>
    </section>

    
@endsection

@section('javascript')
	@parent
@endsection