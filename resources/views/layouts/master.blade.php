<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <script src="{{ asset('vendor/fontawesome-free-5.0.1/svg-with-js/js/fontawesome-all.js') }}"></script>
        
        <!-- Stylesheet -->
        @section('stylesheet')
    		<link rel="stylesheet" href="{{ asset('vendor/ionicons-2.0.1/css/ionicons.min.css') }}" charset="utf-8">
    		<link rel="stylesheet" href="{{ asset('vendor/bootstrap-3.3.7-dist/css/bootstrap.min.css') }}" charset="utf-8">
    		<link rel="stylesheet" href="{{ asset('vendor/easy-autocomplete/easy-autocomplete.min.css') }}" charset="utf-8">
            <link rel="stylesheet" href="{{ asset('css/app.css') }}" charset="utf-8">
		@show

    </head>
    <body>

        @include('components.nav')

        @yield('content')

        
        
        {{-- Javascript Files --}}
        @section('javascript')
            <script src="{{ asset('vendor/jquery-3.2.1.min.js') }}"></script>
            <script src="{{ asset('vendor/easy-autocomplete/jquery.easy-autocomplete.min.js') }}"></script>
            <script src="{{ asset('vendor/greensock-js/greensock-js/src/minified/TweenMax.min.js') }}"></script>
            <script src="{{ asset('vendor/ScrollMagic/scrollmagic/minified/ScrollMagic.min.js') }}"></script>
            <script src="{{ asset('vendor/ScrollMagic/scrollmagic/minified/plugins/animation.gsap.min.js') }}"></script>
            <script src="{{ asset('vendor/ScrollMagic/scrollmagic/minified/plugins/debug.addIndicators.min.js') }}"></script>
            <script src="{{ asset('vendor/bootstrap-3.3.7-dist/js/bootstrap.min.js') }}"></script>
            <script src="{{ asset('js/app.js') }}"></script>
		@show
    </body>
</html>
