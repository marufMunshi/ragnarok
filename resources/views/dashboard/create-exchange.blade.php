@extends('dashboard.layouts.master') 

@section('title', 'Welcome') 

@section('stylesheet') 
    @parent 
@endsection 

@section('content')
<div class="container" id="create-book">

    <div class="row">
        <h1 class="text-center">Complete Exchange Post</h1>
    </div>

    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <form action="{{ route('store-exchange') }}" method="post">

                {{ csrf_field() }}

                <input type="hidden" value="{{ $id }}" name="book_id">

                @if (Auth::user()->mobile)

                    <div class="form-group">
                        <label>Mobile No</label>
                        <input type="text" class="form-control" name="mobile" placeholder="Mobile No" value="{{ Auth::user()->mobile }}">
                    </div>

                @else

                    <div class="form-group">
                        <label>Mobile No</label>
                        <input type="text" class="form-control" name="mobile" placeholder="Mobile No">
                    </div>

                @endif 

                @if (Auth::user()->address)

                <div class="form-group">
                    <label>Address</label>
                    <textarea name="address" class="form-control" cols="30" rows="5" placeholder="Address">{{ Auth::user()->address }}</textarea>
                </div>

                @else

                    <div class="form-group">
                        <label>Address</label>
                        <textarea name="address" class="form-control" cols="30" rows="5" placeholder="Address"></textarea>
                    </div>

                @endif

                <div class="form-group">
                    <label>Note</label>
                    <textarea name="note" class="form-control" cols="30" rows="5" placeholder="Ex. Any Book's of Humayun Ahmed"></textarea>
                </div>

                <div class="form-group">
                    <label for="book-cover">1. Upload Book Image</label>
                    <input type="file" id="book-cover" name="book_image_01">
                    <p class="help-block">Upload a parfect book cover.</p>
                </div>

                <div class="form-group">
                    <label for="book-cover">2. Upload Book Image</label>
                    <input type="file" id="book-cover" name="book_image_02">
                    <p class="help-block">Upload a parfect book cover.</p>
                </div>

                <div class="form-group">
                    <label for="book-cover">3. Upload Book Image</label>
                    <input type="file" id="book-cover" name="book_image_03">
                    <p class="help-block">Upload a parfect book cover.</p>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection 

@section('javascript') 
    @parent 
@endsection