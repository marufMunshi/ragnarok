@extends('dashboard.layouts.master') 

@section('title', 'Welcome') 

@section('stylesheet') 
    @parent 
@endsection 

@section('content')
<div class="container" id="create-book">

    <div class="row">
        <h1 class="text-center">Complete Lend</h1>
    </div>

    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <form action="{{ route('store-borrower') }}" method="post">

                {{ csrf_field() }}

                <input type="hidden" value="{{ $id }}" name="book_id">

                <div class="form-group">
                    <label for="name">Name</label>
                    <input id="borrower-name" type="text" class="form-control" name="name" placeholder="Borrower Name">
                </div>

                <div class="form-group">
                    <label for="mobile">Mobile No</label>
                    <input id="borrower-mobile" type="text" class="form-control" name="mobile" placeholder="Mobile No">
                </div>

                <div class="form-group">
                    <label for="book-name">Email</label>
                    <input id="borrower-email" type="text" class="form-control" name="email" placeholder="Email">
                </div>

                <div class="form-group form-element-desktop">
                    <label class="control-label" for="date">Lend Date</label>
                    <input class="form-control desktop-input date" id="desktop-date-1" name="lend_date" placeholder="MM/DD/YYYY" type="text" readonly="true"/>
                </div>

                <div class="form-group form-element-desktop">
                    <label id="desktop-label-date-2"class="control-label" for="date">Expected Return</label>
                    <input class="form-control desktop-input date" id="desktop-date-2" name="return_date" placeholder="MM/DD/YYYY" type="text" readonly="true"/>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection 

@section('javascript') 
    @parent
    <script>
        const options = {

        url: "/dashboard/borrower-name",

        getValue: "borrower_name",

        list: {

            onSelectItemEvent: function() {
                var value = $("#borrower-name").getSelectedItemData();

                $("#borrower-mobile").val(value.phone_number).trigger("change");
                $("#borrower-email").val(value.email).trigger("change");
            },
            
            match: {
                enabled: true
            }
        }
        };

        $("#borrower-name").easyAutocomplete(options);
    </script>
@endsection