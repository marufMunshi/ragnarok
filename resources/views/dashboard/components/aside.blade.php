<aside class="fixed-left-sidebar"> <!-- Left Sidebar started -->
    <div>
        <ul>

            <li>
                <a href="/dashboard">
                <i class="fas fa-home fa-lg"></i>
                    <span class="left-sidebar-icon-label">Dashboard</span>
                </a>
            </li>

            <li>
                <a href="{{ route('create-book') }}">
                    <i class="fas fa-edit fa-lg"></i>
                    <span class="left-sidebar-icon-label">Create Book</span>
                </a>
            </li>

            <li>
                <a href="{{ route('shelf') }}">
                <i class="fas fa-book fa-lg"></i>
                    <span class="left-sidebar-icon-label">Book Shelf</span>
                </a>
            </li>


            <!-- <li>
                <a href="{{ route('create-book') }}">
                    <i class="fab fa-sellcast fa-lg"></i>
                    <span class="left-sidebar-icon-label">Sell Books</span>
                </a>
            </li>

            <li>
                <a href="{{ route('create-book') }}">
                    <i class="fas fa-exchange-alt fa-lg"></i>
                    <span class="left-sidebar-icon-label">Exchange Books</span>
                </a>
            </li>

            <li>
                <a href="{{ route('create-book') }}">
                    <i class="fas fa-location-arrow fa-lg"></i>
                    <span class="left-sidebar-icon-label">Borrower Books</span>
                </a>
            </li> -->
        </ul>

        <div class="arrow-bottom">

            <i class="ion-ios-arrow-forward"></i>

        </div>
    </div>
</aside> <!-- Left Sidebar ended -->