<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Bookshelf Dashboard</title>

        <script src="{{ asset('vendor/fontawesome-free-5.0.1/svg-with-js/js/fontawesome-all.js') }}"></script>

        @section('stylesheet')
            <!-- Fonts -->
            <link rel="stylesheet" href="{{ asset('vendor/bootstrap-3.3.7-dist/css/bootstrap.min.css') }}" charset="utf-8">
    		<link rel="stylesheet" href="{{ asset('vendor/easy-autocomplete/easy-autocomplete.min.css') }}" charset="utf-8">
    		<link rel="stylesheet" href="{{ asset('vendor/ionicons-2.0.1/css/ionicons.min.css') }}" charset="utf-8">            
            <link rel="stylesheet" href="{{ asset('css/dashboard.css') }}">
            <link rel="stylesheet" href="{{ asset('css/dashboard-query.css') }}">
		@show

    </head>
    <body>

        @include('dashboard.components.nav')

        @include('dashboard.components.aside')


        <div class="top-margin">

            <div class="container">
                <!--Message after oparetion complete -->
                @if ($flash = session('status'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <p><strong>Well done! </strong>{{ $flash }}</p> 
                    </div>
                @endif

                <!--Custom Error Message after oparetion complete -->
                @if ($flash = session('error'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <p><strong>Oh snap! </strong>{{ $flash }}</p>
                    </div>
                @endif

                <!--Error message -->
                @if (count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        @foreach ($errors->all() as $error)
                            <p><strong>Oh snap! </strong>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif
            </div>
        
            @yield('content')
        
        </div>

        {{-- @include('dashboard.components.footer') --}}

        {{-- Javascript Files --}}
        @section('javascript')
            <script src="{{ asset('vendor/jquery-3.2.1.min.js') }}"></script>
            <script src="{{ asset('vendor/bootstrap-3.3.7-dist/js/bootstrap.min.js') }}"></script>
            <script src="{{ asset('vendor/bootstrap-datepicker.min.js') }}"></script>
            <script src="{{ asset('vendor/easy-autocomplete/jquery.easy-autocomplete.min.js') }}"></script>
            <script src="{{ asset('js/dashboard.js') }}"></script>
		@show
    </body>
</html>
