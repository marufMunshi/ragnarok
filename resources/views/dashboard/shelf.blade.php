@extends('dashboard.layouts.master')

@section('title', 'Shelf')

@section('stylesheet')
	@parent
@endsection

@section('content')


    <section class="report-main-content-desktop hidden-xs">
        <div class="container">
            <div class="row">

                <div class="col-sm-12">

                        <form class="form-inline desktop-form">

                            <div id="desktop-select-1" class="form-group form-element-desktop">
                                <label for="">Author</label>
                                <select class="form-control">
                                    <option>Any</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>

                            <div id="desktop-select-2" class="form-group form-element-desktop">
                                <label for="">Category</label>
                                <select class="form-control">
                                    <option>Any</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>

                            <div id="desktop-select-3" class="form-group form-element-desktop">
                                <label for="">Status</label>
                                <select class="form-control">
                                    <option>Any</option>
                                    <option>On Shelf</option>
                                    <option>On Sell</option>
                                    <option>On Exchange</option>
                                </select>
                            </div>

                            <button type="submit" id="dekstop-data-btn" class="btn btn-default btn-success btn-lg">Submit</button>

                        </form>

                    <form class="desktop-" action=""></form>

                </div>

            </div>

            <div class="table-responsive report-table-desktop">
                    <table class="table table-bordered">
                        <caption><h3>All Books</h3></caption>
                        <thead>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Category</th>
                            <th>Language</th>
                            <th>Status</th>
                        </thead>
                
                        @foreach ($books as $book)
                            <tr onclick="window.location='{{ route('dashboard-book', ['id' => $book->id])}}'">
                                
                                <td>{{ $book->title }}</td>
                                <td>{{ $book->authors[0]->author_name }}</td>
                                <td>{{ $book->category->category_name }}</td>
                                <td>{{ $book->language->language_name }}</td>
                                @if ($book->users[0]->pivot->book_sell_status)
                                    <td>Book is on the Sell</td>
                                @elseif ($book->users[0]->pivot->book_exchange_status)
                                    <td>Book is on the Exchange</td>
                                @elseif ($book->users[0]->pivot->book_lending_status)
                                    <td>Lend to Borrower</td>
                                @else
                                    <td>Book is on the Shelf</td>
                                @endif
                                
                            </tr>
                        @endforeach
                
                    </table>

                    <div class="row">
                        <div class="col-md-12 text-center">
                            {{ $books->links() }}
                        </div>
                    </div>

            </div>

        </div>
    </section>


    

@endsection

@section('javascript')
	@parent
@endsection