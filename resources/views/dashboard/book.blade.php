@extends('dashboard.layouts.master')

@section('title', 'Book')

@section('stylesheet')
	@parent
@endsection

@section('content')

    <section id="dashboard-book">
        <div class="container">

            <div class="row section-heading">
                <h1 class="text-center text-capitalize">{{ $book[0]->title }}</h1>
            </div>

            <br><br>

            <div class="row">
                
                <div class="col-sm-12 col-md-3">
                    <div id="book-fixed-section">
                        <div class="book-wrapper">
                            <div class="book-image">
                                <img class="img-responsive center-block" src="/images/book-three.jpg" alt="book cover" />
                            </div>
                            <div class="book-meta text-center text-capitalize">

                                <div class="book-extra-info">
                                    <br>
                                    <p>
                                        <i class="glyphicon glyphicon-pencil"></i>
                                        {{ $book[0]->authors[0]->author_name }}
                                    </p>
                                    <p>
                                        <span class="pull-left">
                                            <i class="glyphicon glyphicon-tasks"></i>
                                            {{ $book[0]->category->category_name }}
                                        </span>
                                        <span class="pull-right">
                                            <i class="glyphicon glyphicon-text-width"></i>
                                            {{ $book[0]->language->language_name }}
                                        </span>
                                    </p>
                                </div>
                                
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-sm-12 col-md-8 col-md-offset-1">
                    <div class="row">

                        <div class="col-md-7">
                            <div class="well">
                    
                                @if ($book[0]->users[0]->pivot->book_lending_status)

                                    <h3 class="text-center">Book Is on the Lend</h3>
                                    <h4>Is Returned?</h4>
                                    <form action="{{ route('change-option') }}" method="post">
                                        {{ csrf_field() }}
                                        
                                        <input type="hidden" value="{{ $book[0]->id }}" name="book_id">
                                        <input type="hidden" value="lend" name="current_option">
                                        <input type="hidden" value="Not Needed" name="book_option">

                                        <div class="form-group form-element-desktop">
                                            <label id="desktop-label-date-2"class="control-label" for="date">Return Date</label>
                                            <input class="form-control desktop-input date" id="desktop-date-2" name="return_date" placeholder="MM/DD/YYYY" type="text" readonly="true"/>
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </form>

                                @elseif ($book[0]->users[0]->pivot->book_sell_status)

                                    <h3 class="text-center">Book Is on the Sell</h3>
                                    <h4>Would You like to change?</h4>
                                    
                                    <form action="{{ route('change-option') }}" method="post">
                                        {{ csrf_field() }}

                                        <input type="hidden" value="{{ $book[0]->id }}" name="book_id">   
                                        <input type="hidden" value="sell" name="current_option">                                                                             

                                        <div class="switch form-group">

                                            <input name="book_option" type="radio" value="shelf" id="optionone">
                                            <label for="optionone">Put on the shelf</label>

                                            <input name="book_option" type="radio" value="exchange" id="optionthree">
                                            <label for="optionthree">Exchange</label>

                                            <input name="book_option" type="radio" value="lend" id="optiontwo">
                                            <label for="optiontwo">Lend</label>

                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </form>

                                @elseif ($book[0]->users[0]->pivot->book_exchange_status)

                                    <h3 class="text-center">Book Is on the Exchange</h3>
                                    <h4>Would You like to change?</h4>
                                    
                                    <form action="{{ route('change-option') }}" method="post">
                                        {{ csrf_field() }}

                                        <input type="hidden" value="{{ $book[0]->id }}" name="book_id">                                        
                                        <input type="hidden" value="exchange" name="current_option">

                                        <div class="switch form-group">

                                            <input name="book_option" type="radio" value="shelf" id="optionone">
                                            <label for="optionone">Put on the shelf</label>

                                            <input name="book_option" type="radio" value="sell" id="optiontwo">
                                            <label for="optiontwo">Sell</label>

                                            <input name="book_option" type="radio" value="lend" id="optionthree">
                                            <label for="optionthree">Lend</label>

                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </form>

                                @else

                                    <h3 class="text-center">Book is on the Shelf</h3>
                                    <h4>Would You like to change?</h4>
                                    
                                    <form action="{{ route('change-option') }}" method="post">
                                        {{ csrf_field() }}

                                        <input type="hidden" value="{{ $book[0]->id }}" name="book_id">                                        
                                        <input type="hidden" value="shelf" name="current_option">

                                        <div class="switch form-group">

                                            <input name="book_option" type="radio" value="sell" id="option_one">
                                            <label for="option_one">Sell</label>

                                            <input name="book_option" type="radio" value="exchange" id="option_two">
                                            <label for="option_two">Exchange</label>

                                            <input name="book_option" type="radio" value="lend" id="option_three">
                                            <label for="option_three">Lend</label>

                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </form>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="well">

                                @if (!$book[0]->users[0]->pivot->publisher)

                                <form action="{{ route('extra-info') }}" method="post">
                                    {{ csrf_field() }}

                                        <input type="hidden" value="{{ $book[0]->id }}" name="book_id">                                    

                                        <div class="form-group">
                                            <label for="publisher">Publisher</label>
                                            <input type="text" class="form-control" name="publisher" placeholder="Publisher">
                                        </div>

                                        <div class="form-group">
                                            <label for="edition">Edition</label>
                                            <input type="text" class="form-control" name="edition" placeholder="Edition">
                                        </div>

                                        <!-- <div class="form-group">
                                            <label for="pub_year">Published Year</label>
                                            <input type="text" class="form-control" name="published_year" placeholder="Published Year">
                                        </div> -->

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>

                                </form>

                                @else

                                    <table class="table">
                                        <tr>
                                            <td>Publisher</td>
                                            <td>{{ $book[0]->users[0]->pivot->publisher }}</td>
                                        </tr>
                                        <tr>
                                            <td>Edition</td>
                                            <td>{{ $book[0]->users[0]->pivot->edition }}</td>
                                        </tr>
                                    </table>
                                @endif

                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

            <br><br>
            <div class="row">
                <div class="col-md-12">
                    @if ($book[0]->ratingsWithUser()->count()) 
                        <div class="panel panel-success">
                            <div class="panel-heading clearfix">
                                <p class="pull-left">Your Review</p>

                                <div class="star-rating pull-right">
                                    @for ($i = 0; $i < $book[0]->ratings[0]->value; $i++)
                                        <i class="ion-ios-star"></i>
                                    @endfor
                                </div>
                                
                            </div>
                            <div class="panel-body">
                                {{ $book[0]->reviews[0]->review }}
                            </div>
                        </div>
                    @else
                        <div class="panel panel-success">
                            
                            <div class="panel-body">
                                <form action="{{ route('store-review') }}" method="post">

                                    {{ csrf_field() }}

                                    <div class="form-group rating">
                                        <label>
                                            <input type="radio" name="rating" value="1" />
                                            <span class="icon">★</span>
                                        </label>
                                        <label>
                                            <input type="radio" name="rating" value="2" />
                                            <span class="icon">★</span>
                                            <span class="icon">★</span>
                                        </label>
                                        <label>
                                            <input type="radio" name="rating" value="3" />
                                            <span class="icon">★</span>
                                            <span class="icon">★</span>
                                            <span class="icon">★</span>   
                                        </label>
                                        <label>
                                            <input type="radio" name="rating" value="4" />
                                            <span class="icon">★</span>
                                            <span class="icon">★</span>
                                            <span class="icon">★</span>
                                            <span class="icon">★</span>
                                        </label>
                                        <label>
                                            <input type="radio" name="rating" value="5" />
                                            <span class="icon">★</span>
                                            <span class="icon">★</span>
                                            <span class="icon">★</span>
                                            <span class="icon">★</span>
                                            <span class="icon">★</span>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Write Review</label>
                                        <textarea class="form-control" name="review" id="" cols="30" rows="10" placeholder="Write Review" required></textarea>
                                    </div>

                                    <input type="hidden" value="{{ $book[0]->id }}" name="book_id">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    @endif
                </div>
            </div>

            <br><br>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered borrower-table">
                        <caption><h3>Borrower List</h3></caption>
                        <thead>
                            <th>Name</th>
                            <th>Lend Date</th>
                            <th>Expected Return Date</th>
                            <th>Return Date</th>
                        </thead>
                
                        
                            @foreach ($borrowers as $borrower)
                                <tr onclick="window.location='{{ route('borrower', ['id' => $borrower->id])}}'">
                                    
                                    <td>{{ $borrower->borrower_name }}</td>
                                    <td>{{ $borrower->pivot->lend_date }}</td>
                                    <td>{{ $borrower->pivot->return_date }}</td>
                                    @if ($borrower->pivot->orginal_return_date)
                                        <td>{{ $borrower->pivot->orginal_return_date }}</td>
                                    @else
                                        <td>Book Not Returned Yet!</td>
                                    @endif
                                    
                                </tr>
                            @endforeach
                        
                
                    </table>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('javascript')
	@parent
@endsection