@extends('dashboard.layouts.master') 

@section('title', 'Welcome') 

@section('stylesheet') 
    @parent 
@endsection 

@section('content')
<div class="container" id="create-book">

    <div class="row">
        <h1 class="text-center">Create Book</h1>
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form action="{{ route('store-book') }}" method="post" enctype="multipart/form-data">

                {{ csrf_field() }}

                <div class="form-group">
                    <label for="book-name">Book Name</label>
                    <input id="book-name" type="text" class="form-control" name="title" placeholder="Book Name">
                </div>

                <div class="form-group">
                    <label for="author">Author</label>
                    <input id="book-author" type="text" class="form-control" name="author" placeholder="Author">
                </div>

                <div class="form-group">
                    <label for="category">Category</label>
                    <input id="book-category" type="text" class="form-control" name="category" placeholder="Category">
                </div>

                <div class="form-group">
                    <label for="language">Language</label>
                    <input id="book-language" type="text" class="form-control" name="language" placeholder="Language">
                </div>

                <div class="form-group">
                    <label for="book-cover">Upload Book Cover</label>
                    <input type="file" id="book-cover" name="book_cover">
                    <p class="help-block">Upload a parfect book cover.</p>
                </div>

                <div class="switch form-group">

                    <input name="book_option" type="radio" value="shelf" id="optionone" checked>
                    <label class="active" for="optionone">Keep on shelf</label>
                    
                    <input name="book_option" type="radio" value="sell" id="optiontwo">
                    <label for="optiontwo">Sell</label>

                    <input name="book_option" type="radio" value="exchange" id="optionthree">
                    <label for="optionthree">Exchange</label>

                    <input name="book_option" type="radio" value="lend" id="optionfour">
                    <label for="optionfour">Lend</label>

                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection 

@section('javascript') 
    @parent 
    <script>

        /*
            Get Books Name & Fill other fields
        */
        const books = {

            url: "/book-name",

            getValue: "title",

            list: {

                onSelectItemEvent: function() {
                    var value = $("#book-name").getSelectedItemData();

                    $("#book-author").val(value.authors[0].author_name).trigger("change");
                    $("#book-category").val(value.category.category_name).trigger("change");
                    $("#book-language").val(value.language.language_name).trigger("change");
                },
                
                match: {
                    enabled: true
                }
            }
        };

        $("#book-name").easyAutocomplete(books);

        /*
            Get Category Name
        */
        const categories = {

            url: "/dashboard/category-name",

            getValue: "category_name",

            list: {
                
                match: {
                    enabled: true
                }
            }
        };

        $("#book-category").easyAutocomplete(categories);

        /*
            Get Language Name
        */
        const languages = {

            url: "/dashboard/language-name",

            getValue: "language_name",

            list: {
                
                match: {
                    enabled: true
                }
            }
        };

        $("#book-language").easyAutocomplete(languages);

        /*
            Get Language Name
        */
        const authors = {

            url: "/dashboard/author-name",

            getValue: "author_name",

            list: {
                
                match: {
                    enabled: true
                }
            }
        };

        $("#book-author").easyAutocomplete(authors);
    </script>
@endsection