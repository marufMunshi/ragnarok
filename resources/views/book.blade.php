@extends('layouts.master')

@section('title', 'Book')

@section('stylesheet')
	@parent
@endsection

@section('content')

    <section id="book">
        <div class="container">
            <div class="row">
                
                <div class="col-sm-12 col-md-4">
                    <div id="book-fixed-section">
                        <div class="book-wrapper">
                            <div class="book-image">
                                @if ($book->users[0]->pivot->image)
                                    <img class="img-responsive center-block" src="/images/books/{{ $book->users[0]->pivot->image }}" alt="book cover" />
                                @else
                                    <img class="img-responsive center-block" src="/images/default-cover.jpg" alt="book cover" />
                                @endif
                            </div>
                            <div class="book-meta text-center text-capitalize">
                                <p>
                                    @if ($bookStatus[0]->book_sell_status)
                                        <em>৳</em>{{ $price[0] }}
                                    @endif
                                </p>
                                <div class="star-rating">
                                    @for ($i = 0; $i < $book->ratings->avg('value'); $i++)
                                        <i class="ion-ios-star"></i>
                                    @endfor
                                </div>
                                

                                <div class="book-extra-info">
                                    <p>
                                        <i class="glyphicon glyphicon-pencil"></i>
                                        {{ $book->authors[0]->author_name }}
                                    </p>
                                    <p>
                                        <span class="pull-left">
                                            <i class="glyphicon glyphicon-tasks"></i>
                                            {{ $book->category->category_name }}
                                        </span>
                                        <span class="pull-right">
                                            <i class="glyphicon glyphicon-text-width"></i>
                                            {{ $book->language->language_name }}
                                        </span>
                                    </p>
                                </div>
                                
                            </div>
                        </div>

                        @if ($bookStatus[0]->book_sell_status)
                            <div class="note-wrap note-blue">
                                <p>{{ $user->name }}</p>
                                <p>{{ $user->address }}</p>
                                <p>{{ $user->mobile }}</p>
                            </div>
                        @elseif ($bookStatus[0]->book_exchange_status)
                            <div class="note-wrap note-blue">
                                <p>{{ $user->name }}</p>
                                <p>{{ $user->address }}</p>
                                <p>{{ $user->mobile }}</p>
                                <p>{{ $note[0] }}</p>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="col-sm-12 col-md-7 col-md-offset-1">

                    <div class="row section-heading">
                        <h1 class="text-center text-capitalize">

                            {{ $book->title }}
                            
                            @if ($bookStatus[0]->book_sell_status)
                                <span class="badge pull-right">Sell</span>
                            @elseif ($bookStatus[0]->book_exchange_status)
                                <span class="badge pull-right">Exchange</span>
                            @endif
                        </h1>
                    </div>

                    @if ($reviews->count())

                        @foreach ($reviews as $review)
                            <div class="panel panel-success">
                                <div class="panel-heading">{{ $review->user->name }}</div>
                                <div class="panel-body">
                                    {{ $review->review }}
                                </div>
                            </div>
                        @endforeach

                    @else 
                        <br><br><br><br>
                        <h2 class="text-center">No One Reviewd Yet!</h2>
                    @endif
                    
                </div>
            </div>
        </div>
    </section>

    
@endsection

@section('javascript')
	@parent
@endsection