@extends('layouts.master')

@section('title', 'Book')

@section('stylesheet')
	@parent
@endsection

@section('content')

    <section id="sell-exchange-books">
        <div class="container">
            <div class="row">

                <div class="section-heading text-center text-capitalize">
                    <h1>New Sell Post</h1>
                </div>

                <div class="row">
                    @foreach ($exchangePosts as $exchangePost)
                
                        <div class="col-xs-6 col-sm-4 col-md-3">

                            <div class="book-wrapper">
                                <a href="">
                                    <div class="book-image">
                                        <img class="img-responsive" src="/images/default-cover.jpg" alt="book cover" />
                                    </div>
                                    <div class="book-meta text-center text-capitalize">
                                        
                                        <div class="star-rating">
                                            @for ($i = 0; $i < $exchangePost->book->ratings->avg('value'); $i++)
                                                <i class="ion-ios-star"></i>
                                            @endfor
                                        </div>
                                        <p>{{ $exchangePost->book->title }}</p>
                                    </div>

                                </a>
                                <div class="book-buttons exchange">
                                    <a href="{{ route('book', ['book-id' => $exchangePost->book_id, 'user-id' => $exchangePost->user_id ])}}">Exchange Now</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="row">
                    <div class="col-md-12 text-center">
                        {{ $exchangePosts->links() }}
                    </div>
                </div>

            </div>
        </div>
    </section>

    
@endsection

@section('javascript')
	@parent
@endsection