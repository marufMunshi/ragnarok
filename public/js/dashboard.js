/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 39);
/******/ })
/************************************************************************/
/******/ ({

/***/ 39:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(40);


/***/ }),

/***/ 40:
/***/ (function(module, exports) {

/*
    Dashboard 
    js 
    start
*/

$(document).ready(function () {
    $('.download-card-selected').addClass('download-card-active');
});

$('#sidebar-reveal').click(function () {

    $('.right-sidebar').toggleClass('reveal-nav');
});

$('.arrow-bottom').click(function () {
    $('.fixed-left-sidebar').toggleClass('fixed-left-extended-sidebar');
    $('.arrow-bottom').addClass('arrow-bottom-extended');

    var icon = $('.arrow-bottom i');

    if (icon.hasClass('ion-ios-arrow-forward')) {
        icon.addClass('ion-ios-arrow-back');
        icon.removeClass('ion-ios-arrow-forward');
    } else {
        icon.addClass('ion-ios-arrow-forward');
        icon.removeClass('ion-ios-arrow-back');
    }
});

var graph = $('#graph-onclick--js'); // graph image id that will be changed on click
var downloadCard = $('.download-card-selected'); // used for active or inactive card 
var bookCard = $('.book-card-selected'); // used for active or inactive card 
var saleCard = $('.sale-card-selected'); // used for active or inactive card 

$('#download-onclick--js').click(function () {
    graph.attr('src', 'src/img/download-graph.png');
    downloadCard.addClass('download-card-active');
    bookCard.removeClass('book-card-active');
    saleCard.removeClass('sale-card-active');
});

$('#book-onclick--js').click(function () {
    graph.attr('src', 'src/img/book-graph.png');
    bookCard.addClass('book-card-active');
    downloadCard.removeClass('download-card-active');
    saleCard.removeClass('sale-card-active');
});

$('#sale-onclick--js').click(function () {
    graph.attr('src', 'src/img/sale-graph.png');
    saleCard.addClass('sale-card-active');
    bookCard.removeClass('book-card-active');
    downloadCard.removeClass('download-card-active');
});

/*
    Dashboard 
    js
    end
*/

/*
    log-in page
    js
    end
*/

/*------------------------------------------------------------------------*/

/*
    report page
    js
    start
*/

$(document).ready(function () {
    var date_input = $("input.date"); //our date input has the name "date"
    var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
    var options = {
        format: 'mm/dd/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
        orientation: 'top right',
        allowInputToggle: true
    };
    date_input.datepicker(options);
});

/*
    report page
    js
    end
*/

var bookOption = $('.switch label');

bookOption.click(function () {
    bookOption.removeClass('active');
    $(this).addClass('active');
});

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAgNzM4Y2ZjMzg0YmU0ODE4ZmYxMTYiLCJ3ZWJwYWNrOi8vLy4vcmVzb3VyY2VzL2Fzc2V0cy9qcy9kYXNoYm9hcmQvZGFzaGJvYXJkLmpzIl0sIm5hbWVzIjpbIiQiLCJkb2N1bWVudCIsInJlYWR5IiwiYWRkQ2xhc3MiLCJjbGljayIsInRvZ2dsZUNsYXNzIiwiaWNvbiIsImhhc0NsYXNzIiwicmVtb3ZlQ2xhc3MiLCJncmFwaCIsImRvd25sb2FkQ2FyZCIsImJvb2tDYXJkIiwic2FsZUNhcmQiLCJhdHRyIiwiZGF0ZV9pbnB1dCIsImNvbnRhaW5lciIsImxlbmd0aCIsInBhcmVudCIsIm9wdGlvbnMiLCJmb3JtYXQiLCJ0b2RheUhpZ2hsaWdodCIsImF1dG9jbG9zZSIsIm9yaWVudGF0aW9uIiwiYWxsb3dJbnB1dFRvZ2dsZSIsImRhdGVwaWNrZXIiLCJib29rT3B0aW9uIl0sIm1hcHBpbmdzIjoiO0FBQUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLG1DQUEyQiwwQkFBMEIsRUFBRTtBQUN2RCx5Q0FBaUMsZUFBZTtBQUNoRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw4REFBc0QsK0RBQStEOztBQUVySDtBQUNBOztBQUVBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7QUM3REE7Ozs7OztBQU1BQSxFQUFFQyxRQUFGLEVBQVlDLEtBQVosQ0FBa0IsWUFBWTtBQUMxQkYsTUFBRSx5QkFBRixFQUE2QkcsUUFBN0IsQ0FBc0Msc0JBQXRDO0FBQ0gsQ0FGRDs7QUFLQUgsRUFBRSxpQkFBRixFQUFxQkksS0FBckIsQ0FBMkIsWUFBWTs7QUFFbkNKLE1BQUUsZ0JBQUYsRUFBb0JLLFdBQXBCLENBQWdDLFlBQWhDO0FBQ0gsQ0FIRDs7QUFLQUwsRUFBRSxlQUFGLEVBQW1CSSxLQUFuQixDQUF5QixZQUFZO0FBQ2pDSixNQUFFLHFCQUFGLEVBQXlCSyxXQUF6QixDQUFxQyw2QkFBckM7QUFDQUwsTUFBRSxlQUFGLEVBQW1CRyxRQUFuQixDQUE0Qix1QkFBNUI7O0FBRUEsUUFBSUcsT0FBT04sRUFBRSxpQkFBRixDQUFYOztBQUVBLFFBQUlNLEtBQUtDLFFBQUwsQ0FBYyx1QkFBZCxDQUFKLEVBQTRDO0FBQ3hDRCxhQUFLSCxRQUFMLENBQWMsb0JBQWQ7QUFDQUcsYUFBS0UsV0FBTCxDQUFpQix1QkFBakI7QUFDSCxLQUhELE1BSUs7QUFDREYsYUFBS0gsUUFBTCxDQUFjLHVCQUFkO0FBQ0FHLGFBQUtFLFdBQUwsQ0FBaUIsb0JBQWpCO0FBQ0g7QUFDSixDQWREOztBQWdCQSxJQUFJQyxRQUFRVCxFQUFFLG9CQUFGLENBQVosQyxDQUFxQztBQUNyQyxJQUFJVSxlQUFlVixFQUFFLHlCQUFGLENBQW5CLEMsQ0FBaUQ7QUFDakQsSUFBSVcsV0FBV1gsRUFBRSxxQkFBRixDQUFmLEMsQ0FBeUM7QUFDekMsSUFBSVksV0FBV1osRUFBRSxxQkFBRixDQUFmLEMsQ0FBeUM7O0FBRXpDQSxFQUFFLHVCQUFGLEVBQTJCSSxLQUEzQixDQUFpQyxZQUFZO0FBQ3pDSyxVQUFNSSxJQUFOLENBQVcsS0FBWCxFQUFrQiw0QkFBbEI7QUFDQUgsaUJBQWFQLFFBQWIsQ0FBc0Isc0JBQXRCO0FBQ0FRLGFBQVNILFdBQVQsQ0FBcUIsa0JBQXJCO0FBQ0FJLGFBQVNKLFdBQVQsQ0FBcUIsa0JBQXJCO0FBQ0gsQ0FMRDs7QUFRQVIsRUFBRSxtQkFBRixFQUF1QkksS0FBdkIsQ0FBNkIsWUFBWTtBQUNyQ0ssVUFBTUksSUFBTixDQUFXLEtBQVgsRUFBa0Isd0JBQWxCO0FBQ0FGLGFBQVNSLFFBQVQsQ0FBa0Isa0JBQWxCO0FBQ0FPLGlCQUFhRixXQUFiLENBQXlCLHNCQUF6QjtBQUNBSSxhQUFTSixXQUFULENBQXFCLGtCQUFyQjtBQUVILENBTkQ7O0FBUUFSLEVBQUUsbUJBQUYsRUFBdUJJLEtBQXZCLENBQTZCLFlBQVk7QUFDckNLLFVBQU1JLElBQU4sQ0FBVyxLQUFYLEVBQWtCLHdCQUFsQjtBQUNBRCxhQUFTVCxRQUFULENBQWtCLGtCQUFsQjtBQUNBUSxhQUFTSCxXQUFULENBQXFCLGtCQUFyQjtBQUNBRSxpQkFBYUYsV0FBYixDQUF5QixzQkFBekI7QUFDSCxDQUxEOztBQVFBOzs7Ozs7QUFTQTs7Ozs7O0FBUUE7O0FBSUE7Ozs7OztBQU9BUixFQUFFQyxRQUFGLEVBQVlDLEtBQVosQ0FBa0IsWUFBWTtBQUMxQixRQUFJWSxhQUFhZCxFQUFFLFlBQUYsQ0FBakIsQ0FEMEIsQ0FDUTtBQUNsQyxRQUFJZSxZQUFZZixFQUFFLHFCQUFGLEVBQXlCZ0IsTUFBekIsR0FBa0MsQ0FBbEMsR0FBc0NoQixFQUFFLHFCQUFGLEVBQXlCaUIsTUFBekIsRUFBdEMsR0FBMEUsTUFBMUY7QUFDQSxRQUFJQyxVQUFVO0FBQ1ZDLGdCQUFRLFlBREU7QUFFVkosbUJBQVdBLFNBRkQ7QUFHVkssd0JBQWdCLElBSE47QUFJVkMsbUJBQVcsSUFKRDtBQUtWQyxxQkFBYSxXQUxIO0FBTVZDLDBCQUFrQjtBQU5SLEtBQWQ7QUFRQVQsZUFBV1UsVUFBWCxDQUFzQk4sT0FBdEI7QUFDSCxDQVpEOztBQWdCQTs7Ozs7O0FBTUEsSUFBTU8sYUFBYXpCLEVBQUUsZUFBRixDQUFuQjs7QUFFQXlCLFdBQVdyQixLQUFYLENBQWlCLFlBQVc7QUFDeEJxQixlQUFXakIsV0FBWCxDQUF1QixRQUF2QjtBQUNBUixNQUFFLElBQUYsRUFBUUcsUUFBUixDQUFpQixRQUFqQjtBQUNILENBSEQsRSIsImZpbGUiOiIvanMvZGFzaGJvYXJkLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gMzkpO1xuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIHdlYnBhY2svYm9vdHN0cmFwIDczOGNmYzM4NGJlNDgxOGZmMTE2IiwiLypcbiAgICBEYXNoYm9hcmQgXG4gICAganMgXG4gICAgc3RhcnRcbiovXG5cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcbiAgICAkKCcuZG93bmxvYWQtY2FyZC1zZWxlY3RlZCcpLmFkZENsYXNzKCdkb3dubG9hZC1jYXJkLWFjdGl2ZScpO1xufSk7XG5cblxuJCgnI3NpZGViYXItcmV2ZWFsJykuY2xpY2soZnVuY3Rpb24gKCkge1xuXG4gICAgJCgnLnJpZ2h0LXNpZGViYXInKS50b2dnbGVDbGFzcygncmV2ZWFsLW5hdicpO1xufSk7XG5cbiQoJy5hcnJvdy1ib3R0b20nKS5jbGljayhmdW5jdGlvbiAoKSB7XG4gICAgJCgnLmZpeGVkLWxlZnQtc2lkZWJhcicpLnRvZ2dsZUNsYXNzKCdmaXhlZC1sZWZ0LWV4dGVuZGVkLXNpZGViYXInKTtcbiAgICAkKCcuYXJyb3ctYm90dG9tJykuYWRkQ2xhc3MoJ2Fycm93LWJvdHRvbS1leHRlbmRlZCcpO1xuXG4gICAgbGV0IGljb24gPSAkKCcuYXJyb3ctYm90dG9tIGknKTtcblxuICAgIGlmIChpY29uLmhhc0NsYXNzKCdpb24taW9zLWFycm93LWZvcndhcmQnKSkge1xuICAgICAgICBpY29uLmFkZENsYXNzKCdpb24taW9zLWFycm93LWJhY2snKTtcbiAgICAgICAgaWNvbi5yZW1vdmVDbGFzcygnaW9uLWlvcy1hcnJvdy1mb3J3YXJkJyk7XG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgICBpY29uLmFkZENsYXNzKCdpb24taW9zLWFycm93LWZvcndhcmQnKTtcbiAgICAgICAgaWNvbi5yZW1vdmVDbGFzcygnaW9uLWlvcy1hcnJvdy1iYWNrJyk7XG4gICAgfVxufSk7XG5cbnZhciBncmFwaCA9ICQoJyNncmFwaC1vbmNsaWNrLS1qcycpOyAvLyBncmFwaCBpbWFnZSBpZCB0aGF0IHdpbGwgYmUgY2hhbmdlZCBvbiBjbGlja1xudmFyIGRvd25sb2FkQ2FyZCA9ICQoJy5kb3dubG9hZC1jYXJkLXNlbGVjdGVkJyk7IC8vIHVzZWQgZm9yIGFjdGl2ZSBvciBpbmFjdGl2ZSBjYXJkIFxudmFyIGJvb2tDYXJkID0gJCgnLmJvb2stY2FyZC1zZWxlY3RlZCcpOyAvLyB1c2VkIGZvciBhY3RpdmUgb3IgaW5hY3RpdmUgY2FyZCBcbnZhciBzYWxlQ2FyZCA9ICQoJy5zYWxlLWNhcmQtc2VsZWN0ZWQnKTsgLy8gdXNlZCBmb3IgYWN0aXZlIG9yIGluYWN0aXZlIGNhcmQgXG5cbiQoJyNkb3dubG9hZC1vbmNsaWNrLS1qcycpLmNsaWNrKGZ1bmN0aW9uICgpIHtcbiAgICBncmFwaC5hdHRyKCdzcmMnLCAnc3JjL2ltZy9kb3dubG9hZC1ncmFwaC5wbmcnKTtcbiAgICBkb3dubG9hZENhcmQuYWRkQ2xhc3MoJ2Rvd25sb2FkLWNhcmQtYWN0aXZlJyk7XG4gICAgYm9va0NhcmQucmVtb3ZlQ2xhc3MoJ2Jvb2stY2FyZC1hY3RpdmUnKTtcbiAgICBzYWxlQ2FyZC5yZW1vdmVDbGFzcygnc2FsZS1jYXJkLWFjdGl2ZScpO1xufSk7XG5cblxuJCgnI2Jvb2stb25jbGljay0tanMnKS5jbGljayhmdW5jdGlvbiAoKSB7XG4gICAgZ3JhcGguYXR0cignc3JjJywgJ3NyYy9pbWcvYm9vay1ncmFwaC5wbmcnKTtcbiAgICBib29rQ2FyZC5hZGRDbGFzcygnYm9vay1jYXJkLWFjdGl2ZScpO1xuICAgIGRvd25sb2FkQ2FyZC5yZW1vdmVDbGFzcygnZG93bmxvYWQtY2FyZC1hY3RpdmUnKTtcbiAgICBzYWxlQ2FyZC5yZW1vdmVDbGFzcygnc2FsZS1jYXJkLWFjdGl2ZScpO1xuXG59KTtcblxuJCgnI3NhbGUtb25jbGljay0tanMnKS5jbGljayhmdW5jdGlvbiAoKSB7XG4gICAgZ3JhcGguYXR0cignc3JjJywgJ3NyYy9pbWcvc2FsZS1ncmFwaC5wbmcnKTtcbiAgICBzYWxlQ2FyZC5hZGRDbGFzcygnc2FsZS1jYXJkLWFjdGl2ZScpO1xuICAgIGJvb2tDYXJkLnJlbW92ZUNsYXNzKCdib29rLWNhcmQtYWN0aXZlJyk7XG4gICAgZG93bmxvYWRDYXJkLnJlbW92ZUNsYXNzKCdkb3dubG9hZC1jYXJkLWFjdGl2ZScpO1xufSk7XG5cblxuLypcbiAgICBEYXNoYm9hcmQgXG4gICAganNcbiAgICBlbmRcbiovXG5cblxuXG5cbi8qXG4gICAgbG9nLWluIHBhZ2VcbiAgICBqc1xuICAgIGVuZFxuKi9cblxuXG5cbi8qLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tKi9cblxuXG5cbi8qXG4gICAgcmVwb3J0IHBhZ2VcbiAgICBqc1xuICAgIHN0YXJ0XG4qL1xuXG5cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgZGF0ZV9pbnB1dCA9ICQoXCJpbnB1dC5kYXRlXCIpOyAvL291ciBkYXRlIGlucHV0IGhhcyB0aGUgbmFtZSBcImRhdGVcIlxuICAgIHZhciBjb250YWluZXIgPSAkKCcuYm9vdHN0cmFwLWlzbyBmb3JtJykubGVuZ3RoID4gMCA/ICQoJy5ib290c3RyYXAtaXNvIGZvcm0nKS5wYXJlbnQoKSA6IFwiYm9keVwiO1xuICAgIHZhciBvcHRpb25zID0ge1xuICAgICAgICBmb3JtYXQ6ICdtbS9kZC95eXl5JyxcbiAgICAgICAgY29udGFpbmVyOiBjb250YWluZXIsXG4gICAgICAgIHRvZGF5SGlnaGxpZ2h0OiB0cnVlLFxuICAgICAgICBhdXRvY2xvc2U6IHRydWUsXG4gICAgICAgIG9yaWVudGF0aW9uOiAndG9wIHJpZ2h0JyxcbiAgICAgICAgYWxsb3dJbnB1dFRvZ2dsZTogdHJ1ZVxuICAgIH07XG4gICAgZGF0ZV9pbnB1dC5kYXRlcGlja2VyKG9wdGlvbnMpO1xufSlcblxuXG5cbi8qXG4gICAgcmVwb3J0IHBhZ2VcbiAgICBqc1xuICAgIGVuZFxuKi9cblxuY29uc3QgYm9va09wdGlvbiA9ICQoJy5zd2l0Y2ggbGFiZWwnKTtcblxuYm9va09wdGlvbi5jbGljayhmdW5jdGlvbigpIHtcbiAgICBib29rT3B0aW9uLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAkKHRoaXMpLmFkZENsYXNzKCdhY3RpdmUnKTtcbn0pXG5cblxuXG5cblxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL3Jlc291cmNlcy9hc3NldHMvanMvZGFzaGJvYXJkL2Rhc2hib2FyZC5qcyJdLCJzb3VyY2VSb290IjoiIn0=